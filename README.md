# Docker PHP FPM For projects

[![pipeline status](https://gitlab.com/DamienDuboeuf/cfdp/docker/php-fpm-projects/badges/master/pipeline.svg)](https://gitlab.com/DamienDuboeuf/cfdp/docker/php-fpm/-/commits/master)

Its docker image php fpm for all projects

## Projects

### Tarification Service

production:

```bash
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm-projects:tarification-service sh
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm-projects:tarification-service-v1.0.0 sh # for specific version
```

development:

```bash
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm-projects:tarification-service-dev sh
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm-projects:tarification-service-dev-v1.0.0  sh # for specific version
```

## Env vars

- `USER_ID=33` UID of php-fpm
- `USER_GID=33` GUID of php-fpm
- `COMPOSER_VERSION=` Set composer version