ARG PHP_VERSION=7.4-fpm

FROM registry.gitlab.com/damienduboeuf/cfdp/docker/php-fpm:${PHP_VERSION}

MAINTAINER Damien DUBOEUF <dduboeuf@cfpd.fr>, Yann BLACHER <yblancher@cfpd.fr>

ARG PKG=
ARG PKG_DEV=
ARG PHP_LIBS=
ARG PHP_LIBS_DEV=
ARG DEFAULT_COMPOSER_VERSION=


RUN apk add --no-cache ${PKG} ${PKG_DEV}

RUN install-php-extensions ${PHP_LIBS} ${PHP_LIBS_DEV}

ENV COMPOSER_VERSION=${DEFAULT_COMPOSER_VERSION}
